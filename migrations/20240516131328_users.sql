CREATE TABLE IF NOT EXISTS users (
  username TEXT PRIMARY KEY,
  user_id UUID NOT NULL,
  password_hash TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS login_events (
  token TEXT PRIMARY KEY,
  user_id UUID NOT NULL,
  created_at TIMESTAMPTZ NOT NULL
);
