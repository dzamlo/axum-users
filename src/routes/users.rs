use axum::{
    extract::Request,
    http::StatusCode,
    middleware::Next,
    response::{IntoResponse, Response},
    routing::post,
    Extension, Json, Router,
};
use axum_extra::{
    headers::{authorization::Bearer, Authorization},
    TypedHeader,
};
use serde::Serialize;

use crate::{
    app_error::AppError,
    users::{LoginDetails, LoginResult, LoginToken, MaybeUserId},
    UsersExtension,
};

pub fn router() -> Router {
    Router::new()
        .route("/", post(create_user))
        .route("/login", post(login))
}

#[derive(Debug, Serialize)]
struct CreateUserResponse {
    token: LoginToken,
}

async fn create_user(
    Extension(users): Extension<UsersExtension>,
    Json(login_details): Json<LoginDetails>,
) -> Result<impl IntoResponse, AppError> {
    users
        .lock()
        .await
        .create_user(&login_details)
        .await
        .map(|token| (StatusCode::CREATED, Json(CreateUserResponse { token })))
}

#[derive(Debug, Serialize)]
struct LoginResponse {
    token: LoginToken,
}

async fn login(
    Extension(users): Extension<UsersExtension>,
    Json(login_details): Json<LoginDetails>,
) -> Result<impl IntoResponse, AppError> {
    dbg!(&login_details);
    match users.lock().await.login(&login_details).await {
        Ok(LoginResult::Success(token)) => Ok((StatusCode::OK, Json(LoginResponse { token }))),
        Ok(LoginResult::Failed) => Err(AppError::LoginFailure),
        Err(e) => Err(e),
    }
}

pub async fn mw_check_login_token(
    Extension(users): Extension<UsersExtension>,
    maybe_bearer: Option<TypedHeader<Authorization<Bearer>>>,
    mut req: Request,
    next: Next,
) -> Result<Response, AppError> {
    let maybe_user_id = match maybe_bearer {
        Some(TypedHeader(Authorization(bearer))) => {
            let token = LoginToken(bearer.token().into());
            users.lock().await.check_login_token(&token).await?
        }
        None => MaybeUserId(None),
    };

    req.extensions_mut().insert(maybe_user_id);

    Ok(next.run(req).await)
}
