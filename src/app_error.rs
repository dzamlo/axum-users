use axum::{http::StatusCode, response::IntoResponse, Json};
use serde::Serialize;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("")]
    Custom(StatusCode, String),
    #[error("internal server error: {0:?}")]
    InternalError(#[from] anyhow::Error),
    #[error("the username or password provided doesn't match the expected pattern")]
    InvalidUsernameOrPassword,
    #[error("login failure")]
    LoginFailure,
    #[error("unauthorized access")]
    Unauthorized,
}

#[derive(Serialize)]
struct ErrorMessage<'a> {
    error_message: &'a str,
}

impl<'a> ErrorMessage<'a> {
    fn new(error_message: &'a str) -> ErrorMessage<'a> {
        ErrorMessage { error_message }
    }
}

///Just a macro to not repeat myself in the into_response method
macro_rules! e {
    ($m:expr) => {
        Json(ErrorMessage::new($m))
    };
}

impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        tracing::error!("An error happened: {}", &self);
        match self {
            AppError::Custom(code, m) => (code, e!(&m)).into_response(),
            AppError::LoginFailure => {
                (StatusCode::UNAUTHORIZED, e!("Wrong username or password")).into_response()
            }
            AppError::Unauthorized => (
                StatusCode::UNAUTHORIZED,
                e!("You need to login to access this url"),
            )
                .into_response(),
            AppError::InternalError(_) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                e!("Internal server error, try again later"),
            )
                .into_response(),
            AppError::InvalidUsernameOrPassword => (
                StatusCode::BAD_REQUEST,
                e!("the username or password provided doesn't match the expected pattern"),
            )
                .into_response(),
        }
    }
}
