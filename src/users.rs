use std::collections::HashMap;

use anyhow::{anyhow, Result};
use chrono::prelude::*;
use getrandom::getrandom;
use password_auth::{generate_hash, verify_password};
use serde::{Deserialize, Serialize};
use sqlx::{prelude::FromRow, PgPool};
use uuid::Uuid;

use crate::app_error::AppError;

const MIN_USERNAME_LEN: usize = 5;
const MAX_USERNAME_LEN: usize = 128;
const MIN_PASSWORD_LEN: usize = 8;
const MAX_PASSWORD_LEN: usize = 128;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct MaybeUserId(pub Option<Uuid>);

#[derive(Debug, Clone, PartialEq, Eq, Hash, FromRow, Serialize)]
pub struct LoginToken(pub String);

impl LoginToken {
    fn new_random() -> Result<LoginToken> {
        let mut buf = [0u8; 256 / 8];
        getrandom(&mut buf)?;
        Ok(LoginToken(hex::encode(buf)))
    }
}

#[derive(Debug, Clone)]
pub enum LoginResult {
    Success(LoginToken),
    Failed,
}

impl LoginResult {
    pub fn token(&self) -> Option<&LoginToken> {
        match self {
            LoginResult::Success(token) => Some(token),
            LoginResult::Failed => None,
        }
    }
    pub fn is_success(&self) -> bool {
        self.token().is_some()
    }
}

#[derive(Debug, Clone, FromRow)]
pub struct User {
    pub user_id: Uuid,
    pub username: String,
    pub password_hash: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct LoginDetails {
    pub username: String,
    pub password: String,
}
fn is_valid_username_char(c: char) -> bool {
    c.is_alphanumeric() || c == '-' || c == '.' || c == '_'
}
impl LoginDetails {
    fn is_valid(&self) -> bool {
        self.username.len() >= MIN_USERNAME_LEN
            && self.username.len() <= MAX_USERNAME_LEN
            && self.password.len() >= MIN_PASSWORD_LEN
            && self.password.len() <= MAX_PASSWORD_LEN
            && self.username.chars().all(is_valid_username_char)
    }
    fn validate(&self) -> Result<(), AppError> {
        if self.is_valid() {
            Ok(())
        } else {
            Err(AppError::InvalidUsernameOrPassword)
        }
    }
}

#[derive(Debug, Clone, FromRow)]
pub struct LoginEvent {
    #[sqlx(flatten)]
    pub token: LoginToken,
    pub user_id: Uuid,
    pub created_at: DateTime<Utc>,
}

pub trait UsersStorage {
    async fn fetch_user_by_username(&mut self, username: &str) -> Result<Option<User>>;
    async fn insert_user(&mut self, user: &User) -> Result<()>;
    async fn fetch_login_event(&mut self, token: &LoginToken) -> Result<Option<LoginEvent>>;
    async fn insert_login_event(&mut self, login_event: &LoginEvent) -> Result<()>;
}

#[derive(Debug)]
pub struct Users<S> {
    users_storage: S,
}

impl<S> Users<S> {
    pub fn new(users_storage: S) -> Users<S> {
        Users { users_storage }
    }
}

impl<S: UsersStorage> Users<S> {
    pub async fn create_user(
        &mut self,
        login_details: &LoginDetails,
    ) -> Result<LoginToken, AppError> {
        login_details.validate()?;
        let password_hash = generate_hash(&login_details.password);
        let user = User {
            user_id: Uuid::new_v4(),
            username: login_details.username.clone(),
            password_hash,
        };
        self.users_storage.insert_user(&user).await?;
        self.login(login_details)
            .await?
            .token()
            .cloned()
            .ok_or(anyhow!("failure during login after user creation").into())
    }

    pub async fn login(&mut self, login_details: &LoginDetails) -> Result<LoginResult, AppError> {
        login_details.validate()?;
        match self
            .users_storage
            .fetch_user_by_username(&login_details.username)
            .await?
        {
            None => Ok(LoginResult::Failed),
            Some(user) => {
                if verify_password(&login_details.password, &user.password_hash).is_err() {
                    return Ok(LoginResult::Failed);
                }
                let login_event = LoginEvent {
                    token: LoginToken::new_random()?,
                    user_id: user.user_id,
                    created_at: Utc::now(),
                };
                self.users_storage.insert_login_event(&login_event).await?;
                Ok(LoginResult::Success(login_event.token))
            }
        }
    }

    pub async fn check_login_token(&mut self, token: &LoginToken) -> Result<MaybeUserId, AppError> {
        let login_event = self.users_storage.fetch_login_event(token).await?;

        Ok(MaybeUserId(login_event.map(|l| l.user_id)))
    }
}

#[derive(Debug, Clone)]
struct InMemoryUsersStorage {
    users: HashMap<String, User>,
    logins: HashMap<LoginToken, LoginEvent>,
}

impl InMemoryUsersStorage {
    pub fn new() -> InMemoryUsersStorage {
        InMemoryUsersStorage {
            users: HashMap::new(),
            logins: HashMap::new(),
        }
    }
}

impl UsersStorage for InMemoryUsersStorage {
    async fn fetch_user_by_username(&mut self, username: &str) -> Result<Option<User>> {
        Ok(self.users.get(username).cloned())
    }

    async fn insert_user(&mut self, user: &User) -> Result<()> {
        if self.users.contains_key(&user.username) {
            Err(anyhow!("username already exist"))
        } else {
            self.users.insert(user.username.clone(), user.clone());
            Ok(())
        }
    }

    async fn fetch_login_event(&mut self, token: &LoginToken) -> Result<Option<LoginEvent>> {
        Ok(self.logins.get(token).cloned())
    }

    async fn insert_login_event(&mut self, login_event: &LoginEvent) -> Result<()> {
        self.logins
            .insert(login_event.token.clone(), login_event.clone());
        Ok(())
    }
}

#[derive(Debug)]
pub struct PostgresUsersStorage {
    pool: PgPool,
}

impl PostgresUsersStorage {
    pub fn new(pool: PgPool) -> PostgresUsersStorage {
        PostgresUsersStorage { pool }
    }
}

impl UsersStorage for PostgresUsersStorage {
    async fn fetch_user_by_username(&mut self, username: &str) -> Result<Option<User>> {
        let maybe_user = sqlx::query_as(
            "SELECT username, user_id, password_hash FROM users WHERE username = $1",
        )
        .bind(username)
        .fetch_optional(&self.pool)
        .await?;
        Ok(maybe_user)
    }

    async fn insert_user(&mut self, user: &User) -> Result<()> {
        sqlx::query("INSERT INTO users (username, user_id, password_hash) VALUES ($1, $2, $3)")
            .bind(&user.username)
            .bind(user.user_id)
            .bind(&user.password_hash)
            .execute(&self.pool)
            .await?;
        Ok(())
    }

    async fn fetch_login_event(&mut self, token: &LoginToken) -> Result<Option<LoginEvent>> {
        let maybe_login_event =
            sqlx::query_as("SELECT token, user_id, created_at FROM login_events WHERE token = $1")
                .bind(&token.0)
                .fetch_optional(&self.pool)
                .await?;
        Ok(maybe_login_event)
    }

    async fn insert_login_event(&mut self, login_event: &LoginEvent) -> Result<()> {
        sqlx::query("INSERT INTO login_events (token, user_id, created_at) VALUES ($1, $2, $3)")
            .bind(&login_event.token.0)
            .bind(login_event.user_id)
            .bind(login_event.created_at)
            .execute(&self.pool)
            .await?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn test_users_with_in_memory_storage() -> Result<()> {
        let storage = InMemoryUsersStorage::new();
        let mut users = Users::new(storage);
        let login_details1 = LoginDetails {
            username: "user1".into(),
            password: "1234".into(),
        };
        let login_details1_wrong_pw = LoginDetails {
            username: "user1".into(),
            password: "12345".into(),
        };
        let login_details2 = LoginDetails {
            username: "user2".into(),
            password: "5678".into(),
        };

        assert!(!users.login(&login_details1).await?.is_success());
        users.create_user(&login_details1).await?;
        assert!(users.create_user(&login_details1).await.is_err());

        let token1 = users
            .login(&login_details1)
            .await?
            .token()
            .cloned()
            .unwrap();
        assert!(!users.login(&login_details1_wrong_pw).await?.is_success());
        let userid1 = users.check_login_token(&token1).await?;
        assert!(userid1.0.is_some());
        assert!(!users.login(&login_details2).await?.is_success());
        users.create_user(&login_details2).await?;
        let token2 = users
            .login(&login_details2)
            .await?
            .token()
            .cloned()
            .unwrap();
        let userid2 = users.check_login_token(&token2).await?;
        assert!(userid2.0.is_some());
        assert_ne!(userid1, userid2);

        let token3 = users
            .login(&login_details1)
            .await?
            .token()
            .cloned()
            .unwrap();
        let userid3 = users.check_login_token(&token3).await?;
        assert_eq!(userid1, userid3);

        Ok(())
    }
}
