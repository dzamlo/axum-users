use std::sync::Arc;

use anyhow::anyhow;
use app_error::AppError;
use axum::{http::StatusCode, middleware, response::IntoResponse, routing::get, Extension, Router};
use sqlx::PgPool;
use tokio::sync::Mutex;
use users::{MaybeUserId, PostgresUsersStorage, Users};

mod app_error;
mod routes;
mod users;

type UsersExtension = Arc<Mutex<Users<PostgresUsersStorage>>>;
#[shuttle_runtime::main]
async fn main(#[shuttle_shared_db::Postgres] pool: PgPool) -> shuttle_axum::ShuttleAxum {
    sqlx::migrate!()
        .run(&pool)
        .await
        .expect("Failed to run migrations");

    let storage = PostgresUsersStorage::new(pool.clone());
    let users = Users::new(storage);
    let users = Arc::new(Mutex::new(users));

    let users_router = routes::users::router();

    let router = Router::new()
        .nest("/users", users_router)
        .route("/restricted", get(test_user_auth))
        .layer(middleware::from_fn(routes::users::mw_check_login_token))
        .layer(Extension(users));

    Ok(router.into())
}

async fn test_user_auth(
    Extension(maybe_user_id): Extension<MaybeUserId>,
) -> Result<impl IntoResponse, AppError> {
    if maybe_user_id.0.is_none() {
        return Err(AppError::Unauthorized);
    }

    Ok(StatusCode::OK)
}
