# Axum user example

A simple example of managing users using `axum`, a rust web framework.

For real world, more complex project you likelsy want to use `axum-login` and `tower-sessions`.

Run the application locally:
```
cargo install cargo-shuttle
cargo shuttle run
```

Create a user:
```
curl --verbose -X POST -H 'content-type: application/json' localhost:8000/users --data '{"username":"user1", "password": "123456789"}'
```
You get a login token in return.

Login with an existing user:
```
curl --verbose -X POST -H 'content-type: application/json' localhost:8000/users/login --data '{"username":"user1", "password": "123456789"}'
```
You get a login token in return.

Access a restricted page (using the token you got when creating a user or logged in):
```
curl --verbose -H "Authorization: Bearer thelongtokenyougot" localhost:8000/restricted
```